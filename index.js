const { request, response } = require("express");
const express = require("express");
//create an application using express

//this creates an express application and stores this as a constant called app.
//app is our server
const app = express()

//for our application server to run, we need t a port to listen to
const port = 3000;

//setup allows application to read the JSON data
//setup for allowing the server to handle data from requests
//methods used from express JS are middlewares
//Middleware is software that providers services outside of what's offered by the operating system
app.use(express.json());
//allows your app to read data from forms
//by default, information received from the url can only be received as a string or an array
//by applying the option of "extended:true" this allows us to receive informtaion in other data type such as an object/boolean etc, which we will use throughout our application
app.use(express.urlencoded({ extended:true }))


//we will create routes
//express has methods corresspodning to each HTTP method
//This route expects to receive a GET request at the base URI '/'
//"http://localhost:3000/"
app.get('/', (request, response)=> {
    //response.send uses the express JS module's method instead to send a response back to the client
    response.send("Hello World")
})

//get method
app.get('/hello', (request, response)=> {
    response.send("Hello from the /hello endpoint!")
})

//post method
app.post('/hello', (request, response)=> {
    response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
})

//for /signup
//mock database
let users = [];

//post method
app.post('/signup', (request, response)=> {
    console.log(request.body)

    //if contents of the rquest body with the property "username" and "password is not empty"
    if(request.body.username !== '' && request.body.password !== ''){
        //this will store the user object sent via POSTMAN to the users array created above
        users.push(request.body)
        //send response
        response.send(`User ${request.body.username} successfully registered!`)
    }else{
        response.send("Please input BOTH username and password!")
    }
})

//update the password of a user that matches the information provided in the client/Postman
app.put('/change-password', (request, response)=>{    //create a for loop that will loop through the elements of the "users" array

    let message;
    for(let i = 0; i < users.length; i++){
        if(request.body.username == users[i].username){

            //changes the password of the user found by the loop into the password provided in the client/Postman
            users[i].password = request.body.password

            //Changes the message to be sent back by the response
            message = `User ${request.body.username}'s password has been updated`
            break;//para di maghang

        }else{
            message  = "user does not exist"
        }
    
    }
    response.send(message)
})

/**
 * Activity:


7. Export the Postman collection and save it inside the root folder of our application.
8. Create a git repository named S29.
9. Initialize a local git repository, add the remote link and push to git with the commit message of s29 Activity.
10. Add the link in Boodle named "29 Introduction to Express js".
 */
//1. Create a GET route that will access the "/home" route that will print out a simple message.
//2. Process a GET request at the "/home" route using postman.
app.get('/home', (request, response)=> {
    response.send("Welcome to my home page!")
})
// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
// 4. Process a GET request at the "/users" route using postman.
app.get('/users', (request, response)=> {
    response.send(users)
})

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
// 6. Process a DELETE request at the "/delete-user" route using postman.
app.delete('/delete-user', (request, response)=>{   
    let message;
    for(let i = 0; i < users.length; i++){
        if(request.body.username == users[i].username){

            users[i].username = request.body.username

            users.splice(i,1)
 
            message = `User ${request.body.username} has been deleted`
            break;

        }else{
            message  = "user does not exist"
        }
    
    }
    response.send(message)
})



app.listen(port, () => console.log(`Server is running at port ${port}`))